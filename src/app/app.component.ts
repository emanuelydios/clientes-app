import {Component, OnInit} from '@angular/core';
import {AuthService} from './usuarios/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Bienvenido a Angular';
  autenticado: boolean;
  curso: string = "Angular con Spring 5";

  profesor: string = "Andrés Guzmán"
  constructor(private authService: AuthService) {
    this.autenticado = this.authService.isAuthenticated();
  }

  ngOnInit(): void {
  }
}
