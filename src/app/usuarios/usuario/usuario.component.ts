import { Component, OnInit } from '@angular/core';
import {Usuario} from '../usuario';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {UsuarioService} from './usuario.service';
import {Producto} from '../../facturas/models/producto';
import {ModalproductoComponent} from '../../directiva/modalproducto/modalproducto.component';
import {ModalusuarioComponent} from './modalusuario/modalusuario.component';
import swal from "sweetalert2";
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuarios: Usuario[] = [];
  habilitar: boolean = true;

  roleUser: boolean;
  roleAdmin: boolean;

  constructor(private usuarioService: UsuarioService,
              public dialog: MatDialog,
              private authService: AuthService) {
    this.roleUser = authService.hasRole('ROLE_USER');
    this.roleAdmin = authService.hasRole('ROLE_ADMIN');
  }

  ngOnInit() {
    this.getUsuarios();
  }

  setHabilitar(): void {
    this.habilitar = (this.habilitar == true) ? false : true;
  }

  openDialogForm(datos: Usuario) {
    const configPopup = new MatDialogConfig();
    configPopup.disableClose = false;
    configPopup.autoFocus = true;
    configPopup.width = '700px';
    configPopup.height = '700px';
    configPopup.data = {usuario: datos};
    const dialogRef = this.dialog.open(ModalusuarioComponent, configPopup);
    dialogRef.afterClosed().subscribe(response => {
      this.getUsuarios();
    });
  }

  getUsuarios() {
    this.usuarioService.getUsuario().subscribe(
      response => {
        this.usuarios = response;
        console.log(this.usuarios);
      }
    );
  }

  async getUsuarioByUsername(username: string) {
    const respuesta = await this.usuarioService.getUsuarioByUsername(username);
    console.log('UsuariobyUsername: ', respuesta);
    this.openDialogForm(respuesta);
  }
  deleteUsuario(username: string): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al usuario ${username}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        await this.usuarioService.delete(username);
        this.getUsuarios();
      }
    });
  }

  /*async deleteUsuario(username: string) {
    await this.usuarioService.delete(username);
    this.getUsuarios();
  }*/
}
