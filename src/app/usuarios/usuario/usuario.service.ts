import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {url_backend} from '../../../Utilitarios/utilitarios';
import {Usuario} from '../usuario';
import {Observable, throwError} from 'rxjs';
import swal from "sweetalert2";
import {Producto} from '../../facturas/models/producto';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private urlEndPoint: string = url_backend + '/api/usuario';
  private urlEndPointRol: string = url_backend + '/api/rol';
  respuesta: any;
  private _usuario: Usuario;

  constructor(private http: HttpClient,
              private router: Router) {
  }
  getUsuario(): Observable<any> {
    return this.http.get<Usuario[]>(this.urlEndPoint).pipe(
      map((response) => response as any)
    );
  }

  async getRoles() {
    await this.http.get<any>(this.urlEndPointRol).toPromise().then(
      response => {
        this.respuesta = response;
      }
    ).catch(e => {
      if (e.status !== 401 && e.error.mensaje) {
        this.router.navigate(['/usuarios']);
        console.error(e.error.mensaje);
      }

      return throwError(e);
    });
    return this.respuesta;
  }

  async create(usuario: Usuario) {
    await this.http.post<any>(this.urlEndPoint, usuario).toPromise().then(
      response => {
        this.respuesta = response;
        this.router.navigate(['/usuarios']);
        swal('Nuevo Usuario', `El Usuario ${usuario.username} ha sido creado con éxito`, 'success');
      }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
    return this.respuesta;
  }
  async update(usuario: Usuario) {
    await this.http.put<any>(`${this.urlEndPoint}/${usuario.username}`, usuario).toPromise().then(
      response => {this.respuesta = response;
        this.router.navigate(['/usuarios']);
        swal('Usuario Modificado', `El Usuario ${usuario.username} ha sido modificado con éxito`, 'success'); }
    ).catch(e => {
      if (e.status === 400) {
        return throwError(e);
      }
      if (e.error.mensaje) {
        console.error(e.error.mensaje);
      }
      return throwError(e);
    });
    return this.respuesta;
  }

  async getUsuarioByUsername(username: string) {
    await this.http.get<any>(`${this.urlEndPoint}/${username}`).toPromise().then(
      response => {
        this.respuesta = response;
      }
    ).catch(e => {
      if (e.status !== 401 && e.error.mensaje) {
        this.router.navigate(['/usuarios']);
        console.error(e.error.mensaje);
      }

      return throwError(e);
    });
    return this.respuesta;
  }
  async delete(username: string) {
    await this.http.delete<any>(`${this.urlEndPoint}/${username}`).toPromise().then(
      response => {
        this.respuesta = response; this.router.navigate(['/usuarios']);
        swal('Usuario Eliminado', `El Usuario ${username} ha sido eliminado con éxito`, 'success'); }
    ).catch(e => {
      if (e.status === 400) {
        return throwError(e);
      }
      if (e.error.mensaje) {
        console.error(e.error.mensaje);
      }
      return throwError(e);
    });
  }
}
