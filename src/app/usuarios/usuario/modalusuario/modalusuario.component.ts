import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UsuarioService} from '../usuario.service';
import {Usuario} from '../../usuario';
import {Rol} from '../rol';
import {Factura} from '../../../facturas/models/factura';
import swal from "sweetalert2";

@Component({
  selector: 'app-modalusuario',
  templateUrl: './modalusuario.component.html',
  styleUrls: ['./modalusuario.component.css']
})
export class ModalusuarioComponent implements OnInit {

  usuario: Usuario = new Usuario();
  roles: Rol[] = [];
  errores: string[];
  modalGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<ModalusuarioComponent>,
              private formBuilder: FormBuilder,
              private usuarioService: UsuarioService,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.cargarRoles();
    this.buildForm();
    if (this.data.usuario) {
      this.formWithData();
    }
  }
  async cargarRoles() {
    this.roles = await this.usuarioService.getRoles();
    console.log('Roles:', this.roles);
  }
  private buildForm() {
    this.modalGroup = this.formBuilder.group({
      nombre: new FormControl('', [Validators.required, Validators.minLength(4)]),
      apellido: new FormControl('', [Validators.required, Validators.minLength(4)]),
      username: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
      enabled: new FormControl('true', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.minLength(4), Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      rol: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  private formWithData() {
    console.log('Data: ', this.data.usuario);
    this.modalGroup.controls['nombre'].setValue(this.data.usuario.nombre);
    this.modalGroup.controls['apellido'].setValue(this.data.usuario.apellido);
    this.modalGroup.controls['username'].setValue(this.data.usuario.username);
    this.modalGroup.controls['password'].setValue(this.data.usuario.password);
    const enabledString = this.data.usuario.enabled;
    this.modalGroup.controls['enabled'].setValue(String(enabledString));
    this.modalGroup.controls['email'].setValue(this.data.usuario.email);
    this.modalGroup.controls['rol'].setValue(this.data.usuario.roles[0].nombre);
    console.log('enabled:', this.modalGroup.get('enabled').value);
  }
  parseUsuario() {
    this.usuario.nombre = this.modalGroup.get('nombre').value;
    this.usuario.apellido = this.modalGroup.get('apellido').value;
    this.usuario.username = this.modalGroup.get('username').value;
    this.usuario.password = this.modalGroup.get('password').value;
    const enabledBoolean = this.modalGroup.get('enabled').value;
    this.usuario.enabled = (enabledBoolean === 'true');
    this.usuario.email = this.modalGroup.get('email').value;
    this.roles.map(r => {
      if (r.nombre.match(this.modalGroup.get('rol').value)) {
        this.usuario.roles.push(r);
      }
    });
  }
  async saveUsuario() {
    this.parseUsuario();
    console.log(this.usuario);
    const response = await this.usuarioService.create(this.usuario);
    this.close();
  }
  async updateUsuario() {
    this.parseUsuario();
    console.log(this.usuario);
    const response = await this.usuarioService.update(this.usuario);
    this.close();
  }
  close() {
    this.dialogRef.close();
  }

}
