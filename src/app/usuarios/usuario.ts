import {Rol} from './usuario/rol';

export class Usuario {
  id: number;
  username: string;
  password: string;
  enabled: boolean;
  nombre: string;
  apellido: string;
  email: string;
  roles: Rol[] = [];
}
