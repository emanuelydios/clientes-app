import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-g-pie',
  templateUrl: './g-pie.component.html',
  styleUrls: ['./g-pie.component.css']
})
export class GPieComponent implements OnInit, OnChanges {

  @Input() nro_encuestados_ciclo:any[] = [];
  doughnutChartLabels: Label[] = [];
  doughnutChartData: MultiDataSet = [
    []
  ];
  doughnutChartType: ChartType = 'doughnut';

  private donutColors=[
    {
      backgroundColor: [
        'rgba(40, 116, 166, 1)',
        'rgba(191, 201, 202, 1)',
        'rgba(241, 148, 138, 1)',
        'rgba(187, 143, 206, 1)',
        'rgba(133, 193, 233, 1)',
        'rgba(115, 198, 182, 1)',
        'rgba(132, 237, 27, 1)',
        'rgba(132, 27, 237, 1)',
        'rgba(237, 198, 132, 1)',
        'rgba(99, 33, 33, 1)',
        'rgba(33, 33, 99, 1)',
        'rgba(33, 99, 33, 1)',
    ]
    }
  ];

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(){
    //console.log("G-Barra OnChange");
    this.doughnutChartLabels = [];
    this.doughnutChartData = [];
    // let data = [];
    // console.log(data);
    console.log('ENCUESTADOS POR DISTRITO: ', this.nro_encuestados_ciclo.length);
    console.log('ENCUESTADOS POR asd: ', this.nro_encuestados_ciclo);
    for (let index = 0; index < this.nro_encuestados_ciclo.length; index++) {
      this.doughnutChartLabels.push('DISTRITO: ' + this.nro_encuestados_ciclo[index].distrito);
      this.doughnutChartData.push(this.nro_encuestados_ciclo[index].total);
    }
    //this.doughnutChartData[0] = data;
    //console.log(this.doughnutChartData);

  }

}
