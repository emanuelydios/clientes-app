import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductoService} from '../producto.service';
import {Router} from '@angular/router';
import {Producto} from '../../facturas/models/producto';
import {DistribuidorService} from '../../distribuidor/distribuidor.service';
import {Distribuidor} from '../../facturas/models/distribuidor';

@Component({
  selector: 'app-modalproducto',
  templateUrl: './modalproducto.component.html',
  styleUrls: ['./modalproducto.component.css']
})
export class ModalproductoComponent implements OnInit {

  producto: Producto = new Producto();
  distribuidores: Distribuidor[] = [];
  errores: string[];
  modalGroup: FormGroup;
  nombreDistribuidor: String[] = [];

  constructor(public dialogRef: MatDialogRef<ModalproductoComponent>,
              private formBuilder: FormBuilder,
              private productoService: ProductoService,
              private distribuidorService: DistribuidorService,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

   ngOnInit() {
    this.cargarDistribuidores();
    this.buildForm();
    if (this.data.producto) {
      this.formWithData();
    }
  }
  async cargarDistribuidores() {
    this.distribuidores = await this.distribuidorService.getDistribuidoresAsync();
    console.log('Distribuidores:', this.distribuidores);
    this.distribuidores.forEach( r => {
      this.nombreDistribuidor.push(r.nombre);
    });
    console.log('Nombres:', this.nombreDistribuidor);
  }

  private buildForm() {
    this.modalGroup = this.formBuilder.group({
      nombre: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]),
      stock: new FormControl('', Validators.required),
      precio: new FormControl('', Validators.required),
      distribuidor: new FormControl('', Validators.required),
      marca: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]),
      descripcion: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(30)])
    });
  }
  private formWithData() {
    this.modalGroup.controls['nombre'].setValue(this.data.producto.nombre);
    this.modalGroup.controls['stock'].setValue(this.data.producto.stock);
    this.modalGroup.controls['precio'].setValue(this.data.producto.precio);
    this.modalGroup.controls['distribuidor'].setValue(this.data.producto.distribuidor.nombre);
    this.modalGroup.controls['marca'].setValue(this.data.producto.marca);
    this.modalGroup.controls['descripcion'].setValue(this.data.producto.descripcion);
  }
  parseProducto() {
    if (this.data.producto) {
      this.producto.id = this.data.producto.id;
    }
    this.producto.nombre = this.modalGroup.get('nombre').value;
    this.producto.stock = this.modalGroup.get('stock').value;
    this.producto.precio = this.modalGroup.get('precio').value;
    this.distribuidores.map(r => {
      if (r.nombre.match(this.modalGroup.get('distribuidor').value)) {
        this.producto.distribuidor = r;
      }
    });
    this.producto.marca = this.modalGroup.get('marca').value;
    this.producto.descripcion = this.modalGroup.get('descripcion').value;
  }
  async saveProducto() {
    this.parseProducto();
    const response = await this.productoService.create(this.producto);
    this.close();
  }
  async updateProducto() {
    this.parseProducto();
    const response = await this.productoService.update(this.producto);
    this.close();
  }
  close() {
    this.dialogRef.close();
  }

}
