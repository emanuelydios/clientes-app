import {Component, OnInit} from '@angular/core';
import {ProductoService} from './producto.service';
import {Producto} from '../facturas/models/producto';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ModaldistribuidorComponent} from '../distribuidor/modaldistribuidor/modaldistribuidor.component';
import {ModalproductoComponent} from './modalproducto/modalproducto.component';
import {AuthService} from '../usuarios/auth.service';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent implements OnInit{

  productos: Producto[] = [];

  habilitar: boolean = true;
  roleUser: boolean;
  roleAdmin: boolean;
  constructor(private productoService: ProductoService,
              public dialog: MatDialog,
              private authService: AuthService) {
    this.roleUser = authService.hasRole('ROLE_USER');
    this.roleAdmin = authService.hasRole('ROLE_ADMIN');
  }

  ngOnInit(): void {
    this.getProductos();
  }
  setHabilitar(): void {
    this.habilitar = (this.habilitar == true) ? false : true;
  }
  openDialogForm(datos: Producto) {
    const configPopup = new MatDialogConfig();
    configPopup.disableClose = false;
    configPopup.autoFocus = true;
    configPopup.width = '700px';
    configPopup.height = '700px';
    configPopup.data = {producto: datos};
    const dialogRef = this.dialog.open(ModalproductoComponent, configPopup);
    dialogRef.afterClosed().subscribe(response => {
      this.getProductos();
    });
  }

  getProductos() {
   this.productoService.getProducto().subscribe(
     response => {
       this.productos = response;
     }
   );
  }
  async getProductoById(id: any) {
    const respuesta = await this.productoService.getProductoByIdAsync(id);
    this.openDialogForm(respuesta);
  }
  async deleteProducto(id: any){
    await this.productoService.delete(id);
    this.getProductos();
  }
}
