import { Injectable } from '@angular/core';
import {url_backend} from '../../Utilitarios/utilitarios';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Producto} from '../facturas/models/producto';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlEndPoint: string = url_backend + '/api/producto';
  respuesta: any;
  constructor(private http: HttpClient,
              private router: Router) { }

  getProducto(): Observable<any> {
    return this.http.get<Producto[]>(this.urlEndPoint).pipe(
      map((response) => response as any)
    );
  }
  async getProductoByIdAsync(id: any) {
    await this.http.get<any>(`${this.urlEndPoint}/${id}`).toPromise().then(
      response => {this.respuesta = response; }
    ).
      catch(e => {
        if (e.status !== 401 && e.error.mensaje) {
          this.router.navigate(['/distribuidor']);
          console.error(e.error.mensaje);
        }

        return throwError(e);
      });
    return this.respuesta;
  }
  async create(producto: Producto) {
    await this.http.post<any>(this.urlEndPoint, producto).toPromise().then(
      response => {this.respuesta = response;
        this.router.navigate(['/directivas']);
        swal('Nuevo Producto', `El Producto ${producto.nombre} ha sido creado con éxito`, 'success'); }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
    return this.respuesta;
  }
  async update(producto: Producto) {
    await this.http.put<any>(`${this.urlEndPoint}/${producto.id}`, producto).toPromise().then(
      response => {this.respuesta = response;
        this.router.navigate(['/directivas']);
        swal('Producto Modificado', `El Producto ${producto.nombre} ha sido modificado con éxito`, 'success'); }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
    return this.respuesta;
  }
  async delete(id: any) {
    await this.http.delete<any>(`${this.urlEndPoint}/${id}`).toPromise().then(
      response => {this.respuesta = response; this.router.navigate(['/directivas']);
        swal('Producto Eliminado', `El Producto ha sido eliminado con éxito`, 'success'); }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
  }

}
