import {Component, OnInit} from '@angular/core';
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import {Usuario} from '../usuarios/usuario';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit{
  title: string = 'Sistema de Facturación';
  isAuthenticated: boolean;
  private _token: string;
  roleUser: boolean;
  roleAdmin: boolean;
  usuario: Usuario;

  constructor(private authService: AuthService, private router: Router) {
    this.preload();
  }
  logout(): void {
    let username = this.authService.usuario.username;
    this.authService.logout();
    this.preload();
    swal('Logout', `Hola ${username}, has cerrado sesión con éxito!`, 'success');
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    this.preload();
    this.isAuthenticated = this.authService.isAuthenticated();
  }
  preload() {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
    console.log('USUARIO Header: ', this.usuario);
    this.roleUser = this.authService.hasRole('ROLE_USER');
    this.roleAdmin = this.authService.hasRole('ROLE_ADMIN');
  }



}
