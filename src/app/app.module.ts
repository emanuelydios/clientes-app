import { BrowserModule } from '@angular/platform-browser';
import {NgModule, LOCALE_ID, APP_INITIALIZER} from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
import { FormComponent } from './clientes/form.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { ClienteService } from './clientes/cliente.service';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatButtonModule, MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule, MatExpansionModule, MatGridListModule,
  MatIconModule, MatListModule, MatMenuModule, MatProgressSpinnerModule,
  MatSidenavModule, MatSnackBarModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { LoginComponent } from './usuarios/login.component';
import { AuthGuard } from './usuarios/guards/auth.guard';
import { RoleGuard } from './usuarios/guards/role.guard';
import { TokenInterceptor } from './usuarios/interceptors/token.interceptor';
import { AuthInterceptor } from './usuarios/interceptors/auth.interceptor';
import { DetalleFacturaComponent } from './facturas/detalle-factura.component';
import { FacturasComponent } from './facturas/facturas.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DistribuidorComponent } from './distribuidor/distribuidor.component';
import { ModaldistribuidorComponent } from './distribuidor/modaldistribuidor/modaldistribuidor.component';
import { ModalproductoComponent } from './directiva/modalproducto/modalproducto.component';
import { UsuarioComponent } from './usuarios/usuario/usuario.component';
import { ModalusuarioComponent } from './usuarios/usuario/modalusuario/modalusuario.component';
import { EncuestaComponent } from './encuesta/encuesta.component';
import {GapiSession} from '../infrastructure/sessions/gapi.session';
import {AppContext} from '../infrastructure/app.context';
import {AppSession} from '../infrastructure/sessions/app.session';
import {FileSession} from '../infrastructure/sessions/file.session';
import {UserSession} from '../infrastructure/sessions/user.session';
import {FileRepository} from '../infrastructure/repositories/file.repository';
import {UserRepository} from '../infrastructure/repositories/user.repository';
import {BreadCrumbSession} from '../infrastructure/sessions/breadcrumb.session';
import {AppRepository} from '../infrastructure/repositories/app.repository';
import { SigninComponent } from './encuesta/signInDrive/signin/signin.component';
import { TabladriveComponent } from './encuesta/tablaDrive/tabladrive/tabladrive.component';
import {VistaTablasComponent} from './components/vista-tablas/vista-tablas.component';
import {CortesiaComponent} from './components/cortesia/cortesia.component';
import {CapRespuestaComponent} from './components/cap-respuesta/cap-respuesta.component';
import {EmpatiaComponent} from './components/empatia/empatia.component';
import {FiabilidadComponent} from './components/fiabilidad/fiabilidad.component';
import {TangibilidadComponent} from './components/tangibilidad/tangibilidad.component';
import {VistaGeneralComponent} from './components/vista-general/vista-general.component';
import {GBarraComponent} from './components/graficos/g-barra/g-barra.component';
import {GPieComponent} from './components/graficos/g-pie/g-pie.component';
import {GTablaComponent} from './components/graficos/g-tabla/g-tabla.component';
import {GTotalComponent} from './components/graficos/g-total/g-total.component';
import {GExpectativasBarComponent} from './components/graficos/g-expectativas-bar/g-expectativas-bar.component';
import {BarraExpectativaComponent} from './components/graficos/barra-expectativa/barra-expectativa.component';
import {BarraConformidadComponent} from './components/graficos/barra-conformidad/barra-conformidad.component';
import {BarraIndicadoresComponent} from './components/barra-indicadores/barra-indicadores.component';
import {BExpectativaComponent} from './components/graficos/b-expectativa/b-expectativa.component';
import {BPercepcionComponent} from './components/graficos/b-percepcion/b-percepcion.component';
import {AdminComponent} from './components/admin/admin.component';
import {ChartsModule} from 'ng2-charts';

registerLocaleData(localeES, 'es');

export function initGapi(gapiSession: GapiSession) {
  return () => gapiSession.initClient();
}

const routes: Routes = [
  { path: '', redirectTo: '/clientes', pathMatch: 'full' },
  { path: 'directivas', component: DirectivaComponent },
  { path: 'distribuidor', component: DistribuidorComponent},
  { path: 'usuarios', component: UsuarioComponent},
  { path: 'clientes', component: ClientesComponent },
  { path: 'signingoogle', component: SigninComponent },
  { path: 'tabladrive', component: TabladriveComponent },
  { path: 'encuesta', component: EncuestaComponent },
  {path: 'home', component: VistaGeneralComponent},
  {path: 'tangibilidad', component: TangibilidadComponent},
  {path: 'fiabilidad', component: FiabilidadComponent},
  {path: 'empatia', component: EmpatiaComponent},
  {path: 'caprespuesta', component: CapRespuestaComponent},
  {path: 'cortesia', component: CortesiaComponent},
  {path: 'tablas', component: VistaTablasComponent},
  { path: 'clientes/page/:page', component: ClientesComponent },
  { path: 'clientes/form', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'clientes/form/:id', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'login', component: LoginComponent },
  { path: 'facturas/:id', component: DetalleFacturaComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_USER' } },
  { path: 'facturas/form/:clienteId', component: FacturasComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FormComponent,
    PaginatorComponent,
    DetalleComponent,
    LoginComponent,
    DetalleFacturaComponent,
    FacturasComponent,
    DistribuidorComponent,
    ModaldistribuidorComponent,
    ModalproductoComponent,
    UsuarioComponent,
    ModalusuarioComponent,
    EncuestaComponent,
    SigninComponent,
    TabladriveComponent,
    VistaGeneralComponent,
    TangibilidadComponent,
    FiabilidadComponent,
    EmpatiaComponent,
    CapRespuestaComponent,
    CortesiaComponent,
    GBarraComponent,
    GPieComponent,
    GTablaComponent,
    GTotalComponent,
    GExpectativasBarComponent,
    BarraExpectativaComponent,
    BarraConformidadComponent,
    BarraIndicadoresComponent,
    BExpectativaComponent,
    BPercepcionComponent,
    VistaTablasComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule, MatDatepickerModule, MatMomentDateModule,
    ReactiveFormsModule, MatAutocompleteModule, MatInputModule, MatFormFieldModule, MatIconModule, MatTableModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatListModule,
    ChartsModule,
    MatGridListModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatMenuModule,
    MatSnackBarModule
  ],
  entryComponents: [
    ModaldistribuidorComponent, ModalproductoComponent, ModalusuarioComponent
  ],
  providers: [ClienteService,
    { provide: LOCALE_ID, useValue: 'es' },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: APP_INITIALIZER, useFactory: initGapi, deps: [GapiSession], multi: true },
    AppContext,
    AppSession,
    FileSession,
    GapiSession,
    UserSession,
    AppRepository,
    BreadCrumbSession,
    FileRepository,
    UserRepository
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
