import { Component, OnInit } from '@angular/core';
import {DistribuidorService} from './distribuidor.service';
import {Distribuidor} from '../facturas/models/distribuidor';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ModaldistribuidorComponent} from './modaldistribuidor/modaldistribuidor.component';
import {refreshDescendantViews} from '@angular/core/src/render3/instructions';
import {AuthService} from '../usuarios/auth.service';

@Component({
  selector: 'app-distribuidor',
  templateUrl: './distribuidor.component.html',
  styleUrls: ['./distribuidor.component.css']
})
export class DistribuidorComponent implements OnInit {

  distribuidores: Distribuidor[] = [];
  habilitar = true;
  roleUser: boolean;
  roleAdmin: boolean;
  constructor(private distribuidorService: DistribuidorService,
              public dialog: MatDialog,
              private authService: AuthService) {
    this.roleUser = authService.hasRole('ROLE_USER');
    this.roleAdmin = authService.hasRole('ROLE_ADMIN');
  }

  ngOnInit() {
    this.getDistribuidores();
  }

  setHabilitar(): void {
    this.habilitar = (this.habilitar == true) ? false : true;
  }

  openDialogForm(datos: Distribuidor) {
    const configPopup = new MatDialogConfig();
    configPopup.disableClose = false;
    configPopup.autoFocus = true;
    configPopup.width = '1000px';
    configPopup.height = '700px';
    configPopup.data = {distribuidor: datos};
    const dialogRef = this.dialog.open(ModaldistribuidorComponent, configPopup);
    dialogRef.afterClosed().subscribe(response => {
      this.getDistribuidores();
    });
  }

  getDistribuidores() {
    this.distribuidorService.getDistribuidores().subscribe(
      response => {
        this.distribuidores = response;
      }
    );
  }
  async getDistribuidorById(id: any) {
    const respuesta = await this.distribuidorService.getDistribuidorByIdAsync(id);
    this.openDialogForm(respuesta);
  }
  async deleteDistribuidor(id: any) {
    await this.distribuidorService.delete(id);
    this.getDistribuidores();
  }

}
