import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Distribuidor} from '../../facturas/models/distribuidor';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DistribuidorService} from '../distribuidor.service';
import swal from "sweetalert2";
import {Router} from '@angular/router';

@Component({
  selector: 'app-modaldistribuidor',
  templateUrl: './modaldistribuidor.component.html',
  styleUrls: ['./modaldistribuidor.component.css']
})
export class ModaldistribuidorComponent implements OnInit {
  Rubros: string[] = ['Bebidas', 'Electrodomésticos', 'Comida', 'Otros'];
  Areas: string[] = ['Norte', 'Sur', 'Este', 'Centro'];
  distribuidor: Distribuidor = new Distribuidor();
  errores: string[];
  modalGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<ModaldistribuidorComponent>,
              private formBuilder: FormBuilder,
              private distribuidorService: DistribuidorService,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.buildForm();
    if (this.data.distribuidor) {
      this.formWithData();
    }
  }
  private buildForm() {
    this.modalGroup = this.formBuilder.group({
      nombre: new FormControl('' , [Validators.required, Validators.minLength(4)]),
      ruc : new FormControl('', Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(11)])),
      direccion : new FormControl('', [Validators.required, Validators.minLength(4)]),
      rubro : new FormControl('Bebidas', Validators.required),
      area : new FormControl('Norte', Validators.required),
      nombreContacto: new FormControl('', [Validators.required, Validators.minLength(4)]),
      apellidoContacto: new FormControl('', [Validators.required, Validators.minLength(4)]),
      telefono: new FormControl('', [Validators.required, Validators.minLength(0), Validators.maxLength(9)]),
      correo: new FormControl('', [
        Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    });
  }

  private formWithData() {
    this.modalGroup.controls['nombre'].setValue(this.data.distribuidor.nombre);
    this.modalGroup.controls['ruc'].setValue(this.data.distribuidor.ruc);
    this.modalGroup.controls['direccion'].setValue(this.data.distribuidor.direccion);
    this.modalGroup.controls['rubro'].setValue(this.data.distribuidor.rubro);
    this.modalGroup.controls['area'].setValue(this.data.distribuidor.areaDistribucion);
    this.modalGroup.controls['nombreContacto'].setValue(this.data.distribuidor.nombreContacto);
    this.modalGroup.controls['apellidoContacto'].setValue(this.data.distribuidor.apellidoContacto);
    this.modalGroup.controls['telefono'].setValue(this.data.distribuidor.telefono);
    this.modalGroup.controls['correo'].setValue(this.data.distribuidor.correo);
  }

  parseDistribuidor() {
    if (this.data.distribuidor) {
      this.distribuidor.id = this.data.distribuidor.id;
    }
    this.distribuidor.nombre = this.modalGroup.get('nombre').value;
    this.distribuidor.ruc = this.modalGroup.get('ruc').value;
    this.distribuidor.direccion = this.modalGroup.get('direccion').value;
    this.distribuidor.rubro = this.modalGroup.get('rubro').value;
    this.distribuidor.areaDistribucion = this.modalGroup.get('area').value;
    this.distribuidor.nombreContacto = this.modalGroup.get('nombreContacto').value;
    this.distribuidor.apellidoContacto = this.modalGroup.get('apellidoContacto').value;
    this.distribuidor.telefono = this.modalGroup.get('telefono').value;
    this.distribuidor.correo = this.modalGroup.get('correo').value;
  }

  saveDistribuidor() {
    this.parseDistribuidor();
    this.distribuidorService.create(this.distribuidor)
      .subscribe(
        distribuidor => {
          this.router.navigate(['/distribuidor']);
          swal('Nuevo cliente', `El Distribuidor ${distribuidor.nombre} ha sido creado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
    this.dialogRef.close();
  }

  async updateDistribuidor() {
    this.parseDistribuidor();
    const response = await this.distribuidorService.update(this.distribuidor);
    this.dialogRef.close();
  }
  close() {
    this.dialogRef.close();
  }

}
