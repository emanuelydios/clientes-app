import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Producto} from '../facturas/models/producto';
import {catchError, map} from 'rxjs/operators';
import {url_backend} from '../../Utilitarios/utilitarios';
import {HttpClient} from '@angular/common/http';
import {Distribuidor} from '../facturas/models/distribuidor';
import {Cliente} from '../clientes/cliente';
import {Router} from '@angular/router';
import swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class DistribuidorService {

  private urlEndPoint: string = url_backend + '/api/distribuidor';
  respuesta: any;
  constructor(private http: HttpClient,
              private router: Router) { }

  getDistribuidores(): Observable<any> {
    return this.http.get<Distribuidor[]>(this.urlEndPoint).pipe(
      map((response) => response as any)
    );
    return this.respuesta;
  }
  async getDistribuidoresAsync() {
    await this.http.get<Distribuidor[]>(this.urlEndPoint).toPromise().then(
      response => {this.respuesta = response; }
    );
    return this.respuesta;
  }
  getDistribuidorById(id): Observable<any> {
    return this.http.get<any>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if (e.status !== 401 && e.error.mensaje) {
          this.router.navigate(['/distribuidor']);
          console.error(e.error.mensaje);
        }

        return throwError(e);
      }));
  }
  async getDistribuidorByIdAsync(id){
    await this.http.get<any>(`${this.urlEndPoint}/${id}`).toPromise().then(
      response => {this.respuesta = response; }
    ).catch(e => {
      if (e.status !== 401 && e.error.mensaje) {
        this.router.navigate(['/distribuidor']);
        console.error(e.error.mensaje);
      }

      return throwError(e);}
      );
    return this.respuesta;
  }
  create(distribuidor: Distribuidor): Observable<any> {
    return this.http.post(this.urlEndPoint, distribuidor)
      .pipe(
        map((response: any) => response.cliente as Distribuidor),
        catchError(e => {
          if (e.status === 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje);
          }
          return throwError(e);
        }));
  }
  async update(distribuidor: Distribuidor) {
    await this.http.put<any>(`${this.urlEndPoint}/${distribuidor.id}`, distribuidor).toPromise().then(
      response => {this.respuesta = response;
        this.router.navigate(['/distribuidor']);
        swal('Cliente Modificado', `El Distribuidor ${distribuidor.nombre} ha sido modificado con éxito`, 'success'); }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
  }
  async delete(id: any) {
    await this.http.delete<any>(`${this.urlEndPoint}/${id}`).toPromise().then(
      response => {this.respuesta = response;
        this.router.navigate(['/distribuidor']);
        swal('Cliente Eliminado', `El Distribuidor ha sido elminado con éxito`, 'success'); }
    ).catch(
      e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }
    );
  }
}
