import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {url_backend, url_encuesta} from '../../Utilitarios/utilitarios';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  private url_backEncuesta = url_encuesta + '/exportarExcel';
  constructor(private http: HttpClient, private router: Router) { }

  subirArchivo(archivo: File): Observable<HttpEvent<{}>> {
    const formData = new FormData();
    formData.append('file', archivo);

    const req = new HttpRequest('POST', `${this.url_backEncuesta}`, formData, {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
      })
    });
    return this.http.request(req);
  }
}
