import {Component, NgZone, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {FileInfo} from '../../../../models/fileInfo';
import {AppContext} from '../../../../infrastructure/app.context';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tabladrive',
  templateUrl: './tabladrive.component.html',
  styleUrls: ['./tabladrive.component.css']
})
export class TabladriveComponent implements OnInit {

  showTable = false;
  dataSource: MatTableDataSource<FileInfo>;
  displayedColumns: string[] = ['icon', 'name', 'modifiedTime', 'size', 'delete', 'download'];
  files: FileInfo[] = [];
  constructor(
    private appContext: AppContext,
    private router: Router,
    private zone: NgZone,
    public dialog: MatDialog,
  ) {
    this.zone.run(() => {
      this.router.navigate(['/tabladrive']);
    });
    this.dataSource = new MatTableDataSource(this.files);
    this.appContext.Session.File.uploadFinished.subscribe(() => {
      this.refresh(this.appContext.Session.BreadCrumb.currentItem.Id);
    });
  }

  ngOnInit() {
    this.refresh('root');
  }
  delete(file: FileInfo) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
      this.appContext.Repository.File.delete(file.Id)
        .then(() => {
          this.zone.run(() => {
            this.dataSource.data = this.files;
            console.log('Delete successfully');
          });
        });
    }
  }
  download(file: FileInfo) {
    console.log('DESCARGANDOOOO');
    const index = this.files.indexOf(file);
    if (index > -1){
      this.appContext.Repository.File.download(file.Id)
        .then(response => {
          window.open(response.result.webContentLink);
          console.log(response);
          this.zone.run(() => {
            this.router.navigate(['/encuesta']);
          });
        });
    }
  }
  refresh(fileId: string) {
    this.appContext.Repository.File.getFiles(fileId)
      .then((res) => {
        this.zone.run(() => {
          this.files = res;
          this.dataSource.data = this.files;
        });
      });
  }

}
