import { Component, OnInit } from '@angular/core';
import {AppContext} from '../../../../infrastructure/app.context';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  constructor(private appContext: AppContext,
              private router: Router) { }

  ngOnInit() {
  }
  signIn() {
    this.appContext.Session.Gapi.signIn()
      .then(() => {
        if (this.appContext.Session.Gapi.isSignedIn) {
          this.router.navigate(['/tabladrive']);
          // console.log(this.appContext.Session.Gapi.isSignedIn);
        }
      });
  }
}
