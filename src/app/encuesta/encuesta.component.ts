import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppContext} from '../../infrastructure/app.context';
import {MatBottomSheet, MatDialog, MatTableDataSource} from '@angular/material';
import {FileInfo} from '../../models/fileInfo';
import {url_backend, url_encuesta} from '../../Utilitarios/utilitarios';
import swal from "sweetalert2";
import {HttpEventType} from '@angular/common/http';
import {Cliente} from '../clientes/cliente';
import {EncuestaService} from './encuesta.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {
  url_back= url_backend;
  url_backEncuesta = url_encuesta;
  private archivoSeleccionado: File;
  progreso = 0;
  nombreArchivo = 'Seleccionar Archivo';
  constructor( private encuestaService: EncuestaService,
               private router: Router
  ) {
  }

  ngOnInit() {
  }
  seleccionarArchivo(event) {
    this.archivoSeleccionado = event.target.files[0];
    this.progreso = 0;
    console.log(this.archivoSeleccionado);
    this.nombreArchivo = this.archivoSeleccionado.name;
    /*if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal('Error seleccionar imagen: ', 'El archivo debe ser del tipo imagen', 'error');
      this.fotoSeleccionada = null;
    }*/
  }
  subirArchivo() {
    if (!this.archivoSeleccionado) {
      swal('Error Upload: ', 'Debe seleccionar un archivo', 'error');
    } else {
      this.encuestaService.subirArchivo(this.archivoSeleccionado)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            swal('Todo Correcto', 'El archivo se ha subido completamente!', 'success');
            this.router.navigate(['/home']);
          }
        });
    }
  }
}
