import {Distribuidor} from './distribuidor';

export class Producto {
  id: number;
  nombre: string;
  stock: string;
  precio: number;
  distribuidor: Distribuidor;
  marca: string;
  descripcion: string;
}
