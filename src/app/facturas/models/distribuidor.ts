export class Distribuidor{
  id: number;
  nombre: string;
  ruc: string;
  rubro: string;
  areaDistribucion: string;
  direccion: string;
  nombreContacto: string;
  apellidoContacto: string;
  telefono: string;
  correo: string;
}
